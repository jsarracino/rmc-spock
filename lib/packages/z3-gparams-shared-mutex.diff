commit e675ca1ad368a4734ccd4ddbadf222c4edc87520
Author: Nathan Pinnow <pinnow2@llnl.gov>
Date:   Mon Nov 7 00:00:00 2022 -0500

    Use std::shared_mutex for gparams instead of exclusive mutex
    
    This makes Z3 significantly faster in some parallel use cases with
    high thread counts.

diff --git a/src/util/gparams.cpp b/src/util/gparams.cpp
index 28ce5a8..cafb43d 100644
--- a/src/util/gparams.cpp
+++ b/src/util/gparams.cpp
@@ -22,8 +22,9 @@ Notes:
 #include "util/mutex.h"
 #include "util/region.h"
 #include "util/map.h"
+#include <shared_mutex>
 
-static DECLARE_MUTEX(gparams_mux);
+static std::shared_mutex* gparams_mux = nullptr;
 
 extern void gparams_register_modules();
 
@@ -180,7 +181,7 @@ public:
     }
 
     void reset() {
-        lock_guard lock(*gparams_mux);
+        std::unique_lock<std::shared_mutex> lock(*gparams_mux);
         m_params.reset();
         for (auto & kv : m_module_params) {
             dealloc(kv.m_value);
@@ -392,7 +393,7 @@ public:
     void set(char const * name, char const * value) {
         std::string m, p;
         normalize(name, m, p);
-        lock_guard lock(*gparams_mux);
+        std::unique_lock<std::shared_mutex> lock(*gparams_mux);
         if (!m[0]) {
             validate_type(p, value, get_param_descrs());
             set(get_param_descrs(), p, value, m);
@@ -432,7 +433,7 @@ public:
     std::string get_value(char const * name) {
         std::string m, p;
         normalize(name, m, p);
-        lock_guard lock(*gparams_mux);
+        std::unique_lock<std::shared_mutex> lock(*gparams_mux);
         symbol sp(p.c_str());
         if (!m[0]) {
             if (m_params.contains(sp)) {
@@ -465,7 +466,7 @@ public:
         params_ref result;
         params_ref * ps = nullptr;
         {
-            lock_guard lock(*gparams_mux);
+            std::unique_lock<std::shared_mutex> lock(*gparams_mux);
             if (m_module_params.find(module_name, ps)) {
                 result.copy(*ps);
             }
@@ -484,7 +485,7 @@ public:
     // -----------------------------------------------
 
     void display(std::ostream & out, unsigned indent, bool smt2_style, bool include_descr) {
-        lock_guard lock(*gparams_mux);
+        std::unique_lock<std::shared_mutex> lock(*gparams_mux);
         out << "Global parameters\n";
         get_param_descrs().display(out, indent + 4, smt2_style, include_descr);
         out << "\n";
@@ -506,7 +507,7 @@ public:
     }
 
     void display_modules(std::ostream & out) {
-        lock_guard lock(*gparams_mux);
+        std::unique_lock<std::shared_mutex> lock(*gparams_mux);
         for (auto & kv : get_module_param_descrs()) {
             out << "[module] " << kv.m_key;
             char const * descr = nullptr;
@@ -518,7 +519,7 @@ public:
     }
 
     void display_module(std::ostream & out, char const* module_name) {
-        lock_guard lock(*gparams_mux);
+        std::unique_lock<std::shared_mutex> lock(*gparams_mux);
         param_descrs * d = nullptr;
         if (!get_module_param_descr(module_name, d)) {
             std::stringstream strm;
@@ -538,7 +539,7 @@ public:
         std::string m, p;
         normalize(name, m, p);
         symbol sp(p.c_str());
-        lock_guard lock(*gparams_mux);
+        std::unique_lock<std::shared_mutex> lock(*gparams_mux);
         out << name << " " << m << " " << p << "\n";
         param_descrs * d;
         if (!m[0]) {
@@ -641,14 +642,14 @@ void gparams::display_parameter(std::ostream & out, char const * name) {
 
 void gparams::init() {
     TRACE("gparams", tout << "gparams::init()\n";);
-    ALLOC_MUTEX(gparams_mux);
+    gparams_mux = alloc(std::shared_mutex);
     g_imp = alloc(imp);
 }
 
 void gparams::finalize() {
     TRACE("gparams", tout << "gparams::finalize()\n";);
     dealloc(g_imp);
-    DEALLOC_MUTEX(gparams_mux);
+    dealloc(gparams_mux);
 }
 
 std::string& gparams::g_buffer() {
