package: z3

versions:
  - 4.5.0     # 2016-11-07
  - 4.6.0     # 2017-12-18
  - 4.7.1     # 2018-05-22
  - 4.8.1     # 2018-10-15
  - 4.8.3     # 2018-11-19
  - 4.8.4     # 2018-12-20
  # - 4.8.5   no such version
  - 4.8.6     # 2019-09-19
  - 4.8.7     # 2019-11-19
  - 4.8.8     # 2020-05-08
  - 4.8.9     # 2020-09-10
  - 4.8.10    # 2021-01-20
  - 4.8.11    # 2021-06-04
  - 4.8.12    # 2021-07-13
  - 4.8.13    # 2021-11-18
  - 4.8.14    # 2021-12-23
  - 4.8.15    # 2022-03-20
  - 4.8.16    # 2022-04-24
  - 4.8.17    # 2022-05-04
  - 4.9.0     # 2022-06-06
  - 4.9.1     # 2022-07-06
  - 4.10.0    # 2022-07-21
  - 4.10.1    # 2022-07-22
  - 4.10.2    # 2022-07-29
  - 4.11.0    # 2022-08-18
  - 4.11.2    # 2022-09-03
  - 4.12.0    # 2023-01-14
  - 4.12.1    # 2023-01-17
  - 4.12.2    # 2023-05-12
  
dependencies:
  - version: ">=4.5.0"
    aliases: []
    install: [ python, c++-compiler, c-compiler ]
    build: [ default-c ]

variables:
  - version: ">=4.5.0"
    url: https://github.com/Z3Prover/z3
    commit: z3-$PACKAGE_VERSION

  - version: ">=4.8.12"
    patches: gparams-shared-mutex

  - version: ">=4.9.0"
    patches: gparams-shared-mutex-4.9

  - version: ">=4.11.0"
    patches: gparams-shared-mutex-4.11

download:
  - version: ">=4.5.0"
    shell: |
      git clone "$url" download
      (cd download && git checkout -b spock $commit)
      (cd download && git archive --format=tar --prefix=download/ HEAD) >download.tar
      gzip -9 <download.tar >download.tar.gz

install:
  - version: ">=4.5.0"
    shell: |
      cc --spock-triplet
      python --version

      cd download

      # Build static and shared libraries both at once
      CXX=c++ CC=cc python scripts/mk_make.py --prefix="$PACKAGE_ROOT" --staticlib
      cd build
      make -j$PARALLELISM
      make install

  - version: ">=4.6.0"
    shell: |
      cc --spock-triplet
      cd download

      # Build shared libraries
      mkdir _build
      cd _build
      cmake .. -DCMAKE_C_COMPILER=cc -DCMAKE_CXX_COMPILER=c++ -DCMAKE_INSTALL_PREFIX="$PACKAGE_ROOT" \
        -DBUILD_LIBZ3_SHARED:BOOL=YES    -DBUILD_PYTHON_BINDINGS:BOOL=NO    -DBUILD_JAVA_BINDINGS:BOOL=NO \
        -DZ3_BUILD_LIBZ3_SHARED:BOOL=YES -DZ3_BUILD_PYTHON_BINDINGS:BOOL=NO -DZ3_BUILD_JAVA_BINDINGS:BOOL=NO \
        -DBUILD_DOCUMENTATION:BOOL=NO \
        -DZ3_BUILD_DOCUMENTATION:BOOL=NO
      make -j$PARALLELISM
      make install

      # Build static libraries
      cd ..
      rm -rf _build
      mkdir _build
      cd _build
      cmake .. -DCMAKE_C_COMPILER=cc -DCMAKE_CXX_COMPILER=c++ -DCMAKE_INSTALL_PREFIX="$PACKAGE_ROOT" \
        -DBUILD_LIBZ3_SHARED:BOOL=NO    -DBUILD_PYTHON_BINDINGS:BOOL=NO    -DBUILD_JAVA_BINDINGS:BOOL=NO \
        -DZ3_BUILD_LIBZ3_SHARED:BOOL=NO -DZ3_BUILD_PYTHON_BINDINGS:BOOL=NO -DZ3_BUILD_JAVA_BINDINGS:BOOL=NO \
        -DBUILD_DOCUMENTATION:BOOL=NO \
        -DZ3_BUILD_DOCUMENTATION:BOOL=NO
      make -j$PARALLELISM
      make install
      
      # For backward compatibility. It would be nice to use a symlink here, but ROSE's mkinstaller doesn't support
      # symlinks.
      if [ ! -d "$PACKAGE_ROOT/lib" ]; then
          (cd "$PACKAGE_ROOT" && cp -pdr lib64 lib)
      fi
